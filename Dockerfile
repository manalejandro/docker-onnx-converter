FROM debian:sid
RUN apt update && apt install -y git python3-pip && apt clean
RUN git clone --depth 1 https://github.com/xenova/transformers.js /transformers.js
WORKDIR /transformers.js/scripts
RUN pip3 install --break-system-packages -r requirements.txt
CMD "--help"
ENTRYPOINT ["python3", "convert.py"]
