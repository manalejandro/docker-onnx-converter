# docker-onnx-converted - convert AI models in [ONNX](https://onnx.ai/) format

## Build

```
docker buildx build -t docker-onnx-converter .
```

## Usage

```
docker run -ti --name docker-onnx-converter --rm -v $PWD/models:/models docker-onnx-converter \
--model_id /models/<path to model> --quantize --task <type of model task>
```

## License

MIT